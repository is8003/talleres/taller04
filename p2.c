#include <stdio.h>

int main(void){
	unsigned int n;		// Declarar variable n.
	unsigned int num;	// Declarar variable num.
	unsigned int s;		// Declarar variable s.

	printf("Ingresa número entero positivo: ");	// Preguntar a usuario por número entero positivo.
	scanf("%u", &n);				// Obtener número entero positivo y asignarlo a variable n.

	num = n;	// Asignar valor de n a num (almacenador temporal para obtener dígitos).

	do{						// Hacer:
		s = 0;					// Asignar s (suma de dígitos) el valor de 0.

		while (num > 0){			// Mientras num sea mayor a 0:

			s += num % 10;			// Sumar a s el módulo  10 de num.

			printf("+%3u\n", num % 10);	// Imprimir  "+ " num % 10.

			num /= 10;			// Dividir num en 10 y asignar resultado a num.
		}

		printf("=%3u\n", s);			// Imprimir valor de s.
		num = s;				// Asignar a num valor de s.

	}while (s > 9);					// Mientras s sea mayor a 9.

	if (s == 9){					// Si s es igual a 9:
		printf("%u es divisible por 9.\n", n);	// Imprimir: número es divisible por 9.
	}
	else {							// De lo contrario:
		printf("%u no es divisible por 9.\n", n);	// Imprimir: número no es divisible por 9.
	}
}
