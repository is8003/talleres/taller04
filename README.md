# Taller04

## Punto 1

En la Isla del Edén viven dos poblaciones de hormigas, las rojas y las enanas.
La población de rojas (**A**) es menor que la población de enanas (**B**). Sin
embargo, la población **A** está creciendo más rápido que la población **B**.
Escriba un programa que solicite al usuario que ingrese la cantidad de hormigas
y la tasa de crecimiento mensual de cada población. El programa debe generar
después de cuántos meses la población **A** será mayor o igual que la población
**B** y las cantidades de hormigas de ambas poblaciones en ese momento. Tenga
en cuenta que cuando las poblaciones de hormigas son superiores a `20000`,
comienza a haber problemas de alimentación lo que hace que se reduzca la tasa
de crecimiento en un `50%` mensual.

Por ejemplo, si la entrada de muestra es: población `A = 5000`, tasa de
crecimiento de la población `A = 5%`, población `B = 8000` y tasa de
crecimiento de la población `B = 2%`, se obtiene que el número de meses
necesario para que la población **A** supere o sea igual a la población **B**
es `17` meses y las cantidades de hormigas serán de `11447` y `11192`
respectivamente.

> **Nota:** No se consideran poblaciones con número decimales, dado que no
puede existir una parte de la hormiga.

### Refinamiento 1

1. Preguntar y obtener de usuario población y tasa de crecimiento mensual de
   cada población. 
1. Si población **A** es mayor que población **B**:
   1. Imprimo: Error, población A es mayor que población B.
1. De lo contrario, si tasa de población **B** es mayor o igual que tasa de
   población **A**:
   1. Imprimo: Error, población B crece más rápido o igual que población A.
1. De lo contrario:
   1. Mientras población **A** sea menor que población **B**:
      1. Calculo incremento de poblaciones para nuevo mes.
      1. Si población **A** es mayor que `20000`:
	 1. Divido incremento **A** en `2`.
      1. Si población **B** es mayor que `20000`:
	 1. Divido incremento **B** en `2`.
      1. Sumo cada población con su respectivo incremento.
      1. Incremento mes en `1`.
   1. Imprimo cantidad de meses transcurridos y total de poblaciones.

### Refinamiento 2

```mermaid
graph TD
    ini(("Inicio"))-->getPobRate[/"Obtener poblaciones<br>y tasas de crecimiento"/]
    getPobRate-->pobSize{pobA > pobB}
    pobSize--"Sí"-->errPob[/"Error: Población A<br>es mayor a B"/]
    errPob-->fin(("Fin"))
    pobSize--"No"-->rateSize{"tasB > tasA"}
    rateSize--"Sí"-->errRate[/"Error: Tasa de crecimiento<br>B es mayor a A"/]
    errRate-->fin
    rateSize--"No"-->whileGrow{"pobA < pobB"}
    whileGrow--"Si"-->calcInc[["Calculo incrementos<br>de población"]]
    calcInc-->hungryA{"pobA > 20000"}
    hungryA--"Sí"-->halfIncA["Incremento A a<br>la mitad"]
    hungryA--"No"-->hungryB{"pobB > 20000"}
    hungryB--"Sí"-->halfIncB["Incremento B a<br>la mitad"]
    hungryB--"No"-->pobInc[["Sumo incrementos<br>a poblaciones"]]
    halfIncA-->hungryB
    halfIncB-->pobInc
    pobInc-->montInc["Incremento<br>mes"]
    montInc-->whileGrow
    whileGrow--"No"-->printResults[/"Imprimo<br>resultados"/]
    printResults-->fin
```

1. Preguntar a usuario población **A**.
1. Obtener población **A** y asignar a `pobA`.
1. Preguntar a usuario tasa de crecimiento de población **A**.
1. Obtener tasa de crecimiento de población **A** y asignar a `tasA`.
1. Preguntar a usuario población **B**.
1. Obtener población **B** y asignar a `pobB`.
1. Preguntar a usuario tasa de crecimiento de población **B**.
1. Obtener tasa de crecimiento de población **B** y asignar a `tasB`.
1. Si `pobA` es mayor que `pobB`:
   1. Imprimo: Error, población A es mayor que población B.
1. De lo contrario, si `tasB` es mayor o igual que `tasA`:
   1. Imprimo: Error, población B crece más rápido o igual que población A.
1. De lo contrario:
   1. Mientras `pobA` sea menor que `pobA`:
      1. Asigno a `incA` (incremento de mes actual para poblacion **A**) el
	 resultado de `(pobA * tasA) * 0.01`
      1. Asigno a `incB` (incremento de mes actual para poblacion **B**) el
	 resultado de `(pobB * tasB) * 0.01`
      1. Si `pobA` es mayor que `20000`:
	 1. Asigno a `incA` su valor multiplicado por `0.5`.
      1. Si `pobB` es mayor que `20000`:
	 1. Asigno a `incB` su valor multiplicado por `0.5`.
      1. Asigno a `pobA` su valor mas `incA`.
      1. Asigno a `pobB` su valor mas `incB`.
      1. Incremento `mes` en `1`.
   1. Imprimo cantidad de meses transcurridos.
   1. Imprimo cantidad de población **A**.
   1. Imprimo cantidad de población **B**.

### Refinamiento 3

1. Declaro variable `pobA` (población de hormigas rojas).
1. Declaro variable `tasA` (tasa de crecimiento de población de hormigas
   rojas).
1. Declaro variable `pobB` (población de hormigas enanas).
1. Declaro variable `tasB` (tasa de crecimiento de población de hormigas
   enanas).
1. Declaro variable `mes` (tiempo trascurrido en meses) y asigno valor `0`.
1. Preguntar a usuario población **A**.
1. Obtener población **A** y asignar a `pobA`.
1. Preguntar a usuario tasa de crecimiento de población **A**.
1. Obtener tasa de crecimiento de población **A** y asignar a `tasA`.
1. Preguntar a usuario población **B**.
1. Obtener población **B** y asignar a `pobB`.
1. Preguntar a usuario tasa de crecimiento de población **B**.
1. Obtener tasa de crecimiento de población **B** y asignar a `tasB`.
1. Si `pobA` es mayor que `pobB`:
   1. Imprimo: Error, población A es mayor que población B.
1. De lo contrario, si `tasB` es mayor o igual que `tasA`:
   1. Imprimo: Error, población B crece más rápido o igual que población A.
1. De lo contrario:
   1. Mientras `pobA` sea menor que `pobA`:
      1. Declaro variable `incA` (crecimiento de población de hormigas rojas
	 para mes actual).
      1. Declaro variable `incB` (crecimiento de población de hormigas enanas
	 para mes actual).
      1. Asigno a `incA` (incremento de mes actual para poblacion **A**) el
	 resultado de `(pobA * tasA) * 0.01`
      1. Asigno a `incB` (incremento de mes actual para poblacion **B**) el
	 resultado de `(pobB * tasB) * 0.01`
      1. Si `pobA` es mayor que `20000`:
	 1. Asigno a `incA` su valor multiplicado por `0.5`.
      1. Si `pobB` es mayor que `20000`:
	 1. Asigno a `incB` su valor multiplicado por `0.5`.
      1. Asigno a `pobA` su valor mas `incA`.
      1. Asigno a `pobB` su valor mas `incB`.
      1. Incremento `mes` en `1`.
   1. Imprimo valor de `mes` como tiempo transcurrido.
   1. Imprimo valor de `pobA` como población total de hormigas rojas.
   1. Imprimo valor de `pobB` como población total de hormigas enanas.

## Punto 2

Sea n = a<sub>k</sub> a<sub>k-1</sub> a<sub>k-2</sub> ... a<sub>1</sub>
a<sub>0</sub> un número entero, donde a<sub>k</sub> a<sub>k-1</sub>
a<sub>k-2</sub> ... a<sub>1</sub> a<sub>0</sub> representan cada uno de los
dígitos del número n. Sea s = a<sub>k</sub> + a<sub>k-1</sub> + a<sub>k-2</sub>
\+ ... + a<sub>1</sub> + a<sub>0</sub> la suma de los dígitos de n. Se sabe que
n es divisible entre 9 si y sólo si al realizar sumas sucesivas de los dígitos
de s hasta obtener un solo dígito, éste último es igual a 9.

Por ejemplo, suponga que `n = 27193257`. Entonces, el primer 
`s = 2 + 7 + 1 + 9 + 3 + 2 + 5 + 7 = 36`. Como el resultado tiene dos dígitos,
se suman nuevamente los dígitos obteniendo un nuevo `s = 3 + 6 = 9`. Dado que
este resultado tiene un único dígito y es igual a `9`, se puede deducir que
`27193257` es divisible por `9`.

Escriba un programa que le solicite al usuario que ingrese un número entero
positivo y luego use el criterio anterior para determinar si el número es
divisible entre `9`. Adicionalmente, se debe mostrar por pantalla el resultado
de cada iteración.

### Refinamiento 1

1. Obtener número entero positivo de usuario.
1. Hacer:
   1. Calcular e imprimir suma de dígitos.
   1. Mientras suma de díginos sea mayor a 9.
1. Si suma de dígitos es igual a 9:
   1. Imprimir: número es divisible por 9.
1. De lo contrario:
   1. Imprimir: número no es divisible por 9.

### Refinamiento 2

```mermaid
graph TD
    ini(("Inicio"))-->getNum[/"Obtego: número<br>positivo n"/]
    getNum-->numIni[num = n]
    numIni-->sum0["suma = 0"]
    sum0-->numCond{"num > 0"}
    numCond--"Sí"-->sumDig["s += num % 10"]
    sumDig-->printDig[/"Imprimo: num % 10"/]
    printDig-->numDeg["num /= 10"]
    numDeg-->numCond
    numCond--"No"-->printSum[/"Imprimo: s"/]
    printSum-->swapNum["num = s"]
    swapNum-->condRep{"s > 9"}
    condRep--"Sí"-->sum0
    condRep--"No"-->cond9{"s == 9"}
    cond9--"Sí"-->print9[/"Imprimo: n es<br>múltiplo de 9."/]
    print9-->fin(("Fin"))
    cond9--"No"-->printNo9[/"Imprimo: n no es<br>múltiplo de 9."/]  
    printNo9-->fin
```

1. Preguntar a usuario por número entero positivo.
1. Obtener número entero positivo y asignarlo a variable `n`.
1. Asignar valor de `n` a `num` (almacenador temporal para obtener dígitos).
1. Hacer:
   1. Asignar `s` (suma de dígitos) el valor de 0.
   1. Mientras `num` sea mayor a `0`:
      1. Sumar a `s` el módulo  `10` de `num`.
      1. Imprimir  "+ " `num % 10`.
      1. Dividir `num` en `10` y asignar resultado a `num`.
   1. Imprimir valor de `s`.
   1. Asignar a `num` valor de `s`.
   1. Mientras `s` sea mayor a 9.
1. Si `s` es igual a 9:
   1. Imprimir: número es divisible por 9.
1. De lo contrario:
   1. Imprimir: número no es divisible por 9.

### Refinamiento 3

1. Declarar variable `n`.
1. Declarar variable `num`.
1. Declarar variable `s`.
1. Preguntar a usuario por número entero positivo.
1. Obtener número entero positivo y asignarlo a variable `n`.
1. Asignar valor de `n` a `num` (almacenador temporal para obtener dígitos).
1. Hacer:
   1. Asignar `s` (suma de dígitos) el valor de 0.
   1. Mientras `num` sea mayor a `0`:
      1. Sumar a `s` el módulo  `10` de `num`.
      1. Imprimir  "+ " `num % 10`.
      1. Dividir `num` en `10` y asignar resultado a `num`.
   1. Imprimir valor de `s`.
   1. Asignar a `num` valor de `s`.
   1. Mientras `s` sea mayor a 9.
1. Si `s` es igual a 9:
   1. Imprimir: número es divisible por 9.
1. De lo contrario:
   1. Imprimir: número no es divisible por 9.
