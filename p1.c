#include <stdio.h>

int main(void){
	unsigned int pobA;	// Declaro variable pobA (población de hormigas rojas).
	unsigned int tasA;	// Declaro variable tasA (tasa de crecimiento de población de hormigas rojas).
	unsigned int pobB;	// Declaro variable pobB (población de hormigas enanas).
	unsigned int tasB;	// Declaro variable tasB (tasa de crecimiento de población de hormigas enanas).
	unsigned int mes = 0;	// Declaro variable mes (tiempo trascurrido en meses) y asigno valor 0.

	printf("%s", "Ingresa población de hormigas rojas: ");	// Preguntar a usuario población A.
	scanf("%u", &pobA);					// Obtener población A y asignar a pobA.

	printf("%s", "Ingresa tasa de crecimiento de hormigas rojas [%]: ");	// Preguntar a usuario tasa de crecimiento de población A.
	scanf("%u", &tasA);							// Obtener tasa de crecimiento de población A y asignar a tasA.

	printf("%s", "Ingresa población de hormigas enanas: ");	// Preguntar a usuario población B.
	scanf("%u", &pobB);					// Obtener población B y asignar a pobB.

	printf("%s", "Ingresa tasa de crecimiento de hormigas enanas [%]: ");	// Preguntar a usuario tasa de crecimiento de población B.
	scanf("%u", &tasB);							// Obtener tasa de crecimiento de población B y asignar a tasB.

	if (pobA > pobB){								// Si pobA es mayor que pobB:
		puts("Error: Población de hormigas rojas es mayor a población "\
				"de hormigas enanas");					// Imprimo: Error, población A es mayor que población B.
	}
	else if (tasB >= tasA){							// De lo contrario, si tasB es mayor o igual que tasA:
		puts("Error: Población de hormigas enanas crece más rápido o "\
				"igual que población de hormigas enanas");	// Imprimo: Error, población B crece más rápido o igual que población A.
	}
	else{						// De lo contrario:
		while(pobA < pobB){			// Mientras pobA sea menor que pobA:
			unsigned int incA;		// Declaro variable incA (crecimiento de población de hormigas rojas para mes actual).
			unsigned int incB;		// Declaro variable incB (crecimiento de población de hormigas enanas para mes actual).

			incA = (pobA * tasA) * 0.01;	// Asigno a incA (incremento de mes actual para poblacion A) el resultado de pobA * tasA / 100
			incB = (pobB * tasB) * 0.01;	// Asigno a incB (incremento de mes actual para poblacion B) el resultado de pobB * tasB / 100

			if(pobA > 20000){		// Si pobA es mayor que 20000:
				incA *= 0.5;		// Asigno a incA su valor divido en 2.
			}

			if(pobB > 20000){		// Si pobB es mayor que 20000:
				incB *= 0.5;		// Asigno a incB su valor divido en 2.
			}

			pobA += incA;			// Asigno a pobA su valor mas incA.
			pobB += incB;			// Asigno a pobB su valor mas incB.
			mes++;				// Incremento mes en 1.
		}

		printf("Meses transcurridos: %u\n", mes);		// Imprimo cantidad de mes como tiempo transcurrido.
		printf("Población hormigas rojas: %u\n", pobA);		// Imprimo valor de pobA como población total de hormigas rojas.
		printf("Población hormigas enanas: %u\n", pobB);	// Imprimo valor de pobB como población total de hormigas enanas.
	}
}
